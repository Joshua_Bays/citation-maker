#include <stdio.h>

_Bool streq(const char* str1, const char* str2, unsigned len);
unsigned strlength(const char* str);
void mla_cite(const char* firstName, const char* lastName, const char* title, const char* website, const char* publisher, const char* date, const char* url);
void strcopy(const char* src, char* dest, unsigned start, unsigned end);

int main(int argc, char** argv){
	if(argc != 2){
		printf("Please provide one (and only one) argument. Exiting.\n");
		return 1;
	}
	
	_Bool newCitation = 0;
	char lastName[1024];
	char firstName[1024];
	char title[1024];
	char website[1024];
	char publisher[1024];
	char date[1024];
	char url[1024];
	
	FILE* readFile = fopen(argv[1], "r");
	char buff[1024];
	
	fgets(buff, sizeof(buff), readFile);
	while(!feof(readFile)){
		if(streq(buff, "Last name: ", 11)){
			strcopy(buff, lastName, 11, strlength(buff) - 1); newCitation = 1;
		}else if(streq(buff, "First name: ", 12)){
			strcopy(buff, firstName, 12, strlength(buff) - 1); newCitation = 1;
		}else if(streq(buff, "Title: ", 7)){
			strcopy(buff, title, 7, strlength(buff) - 1); newCitation = 1;
		}else if(streq(buff, "Website: ", 9)){
			strcopy(buff, website, 9, strlength(buff) - 1); newCitation = 1;
		}else if(streq(buff, "Publisher: ", 11)){
			strcopy(buff, publisher, 11, strlength(buff) - 1); newCitation = 1;
		}else if(streq(buff, "Date: ", 6)){
			strcopy(buff, date, 6, strlength(buff) - 1); newCitation = 1;
		}else if(streq(buff, "Url: ", 5)){
			strcopy(buff, url, 5, strlength(buff) - 1); newCitation = 1;
		}
		if(streq(buff, "!!!", 3)){
			mla_cite(firstName, lastName, title, website, publisher, date, url); newCitation = 0;
		}
	
		fgets(buff, sizeof(buff), readFile);
	}
	
	if(newCitation){
		mla_cite(firstName, lastName, title, website, publisher, date, url);
	}
	
	/*
	printf("The author's first name is: %s\n", firstName);
	printf("The author's last name is: %s\n", lastName);
	printf("The title is: %s\n", title);
	printf("The website is %s\n", website);
	printf("The publisher is %s\n", publisher);
	printf("The date of access is %s\n", date);
	printf("The url is %s\n", url);
	*/
	
	return 0;
}

_Bool streq(const char* str1, const char* str2, unsigned len){
	for(int i = 0; i < len; i++){
		if(str1[i] != str2[i]){
			return 0;
		}
	}
	return 1;
}

unsigned strlength(const char* str){
	unsigned ret = 0;
	while(str[ret] != '\0'){
		ret++;
	}
	return ret;
}

void mla_cite(const char* firstName, const char* lastName, const char* title, const char* website, const char* publisher, const char* date, const char* url){
	if(strlength(lastName) > 0){ printf("%s", lastName); }
	if(strlength(firstName) > 0){ printf(", %s", firstName); }
	printf(". ");
	if(strlength(title) > 0){ printf("\"%s.\"", title);  }
	if(strlength(website) > 0){ printf(", %s", title);  }
	if(strlength(publisher) > 0){ printf(", %s", publisher);  }
	if(strlength(date) > 0){ printf(", %s", date);  }
	if(strlength(url) > 0){ printf(", %s", url); }
	printf(".\n");
}

void strcopy(const char* src, char* dest, unsigned start, unsigned end){
	for(int i = start; i <= end; i++){
		dest[i - start] = src[i];
	}
	dest[end - start] = '\0';
}
